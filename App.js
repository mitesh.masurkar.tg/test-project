import 'react-native-gesture-handler';
import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ChooseImage from './Screen/ChooseImage';
import EditDetail from './Screen/EditDetail';
import StartingScreen from './Screen/StartingScreen';
import image from './image/image.jpg';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const App = () => {
  const Stack = createStackNavigator();

  // const makeRequest = () => {
  //   var myHeaders = new Headers();

  //   myHeaders.append('Authorization', 'a4b2eda0-8ed7-47de-9c72-adf4ecfb3fc0');
  // myHeaders.append(
  //   'Cookie',
  //   '__cfduid=d984a4696bf54d15f3e75755764bc57aa1601003842',
  // );

  // var photo = {
  //   uri: './image/image.jpg',
  //   type: 'image/jpeg',
  //   name: 'image.jpg',
  // };

  // var form = new FormData();
  // form.append('image', photo);
  // const res = await DocumentPicker.pick({
  //   type: [DocumentPicker.types.images],
  // });
  // console.log(
  //   res.uri,
  //   res.type, // mime type
  //   res.name,
  //   res.size
  // );
  // var requestOptions = {
  //   method: 'POST',
  //   headers: myHeaders,
  //   body: form,
  // };

  // fetch('https://app.covve.com/api/businesscards/scan', requestOptions)
  //   .then((response) => response.text())
  //   .then((result) => console.log(result))
  //   .catch((error) => console.log('error', error.message));
  // };

  // makeRequest();
  return (
    <NavigationContainer>
      <View style={{flex: 1}}>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={StartingScreen} />

          <Stack.Screen name="chooseImage" component={ChooseImage} />
          <Stack.Screen name="Edit" component={EditDetail} />
        </Stack.Navigator>
      </View>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});
