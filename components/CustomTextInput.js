import React, {useState} from 'react'
import {StyleSheet, Text, TextInput, View} from 'react-native'
import colors from '../assets/colors'

const CustomTextInput = props => {
  return (
    <View style={styles.CustomTextInput}>
      <View style={styles.text_Container}>
        <Text>{props.title}</Text>
      </View>
      <View>
        <TextInput
          style={styles.textInput}
          defaultValue={props.value}
          onChangeText={props.changeText}
          {...props}
        />
      </View>
    </View>
  )
}

export default CustomTextInput

const styles = StyleSheet.create({
  CustomTextInput: {
    padding: 5,
    margin: 5,
  },
  text_Container: {
    paddingVertical: 5,
  },
  textInput: {
    backgroundColor: colors.background,
    borderBottomColor: colors.accent,
    borderBottomWidth: 1,
    paddingVertical: 2,
  },
})
