import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

const CustomButton = (props) => {
  return (
    <View style={({...props.style}, styles.button)}>
      <Button onPress={props.onPress} title={props.title} />
    </View>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  button: {
    margin: 5,
    maxWidth: 300,
    width: 'auto',
  },
});
