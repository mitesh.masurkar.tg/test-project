import React from 'react';
import {Button, StyleSheet, Text, View, Image} from 'react-native';
import CustomButton from '../components/CustomButton';
import image from '../image/image.jpg';
const ChooseImage = (props) => {
  const {button} = props.route.params;
  console.log(props.route);
  console.log(button);
  const makeRquest = () => {
    props.navigation.navigate('Edit', {
      button: button,
    });
  };

  return (
    <View style={styles.container}>
      <View>
        <Text>{image.url}</Text>
      </View>
      <View style={styles.image_container}>
        <Image source={require('../image/image.jpg')} style={styles.image} />
      </View>

      <View>
        <CustomButton title="Select Image From gallary" />
        <CustomButton title="Select From Camera" />
      </View>

      <View>
        <CustomButton title="Upload" onPress={makeRquest} />
      </View>
    </View>
  );
};

export default ChooseImage;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image_container: {
    elevation: 3,
    width: 200,
    height: 200,
    margin: 50,
  },
  image: {
    borderRadius: 10,

    width: '100%',
    height: '100%',
    aspectRatio: 1,
  },
});
