import React, {useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import CustomButton from '../components/CustomButton';

const StartingScreen = (props) => {
  const buttons = ['Contacts', 'Business', 'Contacts2'];
  const [selectedButton, setselectedButton] = useState('');
  const handleButton = (button) => {
    setselectedButton(button);
    console.log('inside ' + selectedButton);
    props.navigation.navigate('chooseImage', {
      button: selectedButton,
    });
  };
  console.log('outside ' + selectedButton);

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.text}>Type of Card</Text>
      </View>

      <View style={styles.buttons_container}>
        {buttons.map((button) => (
          <View style={styles.button} key={button}>
            <CustomButton
              title={button}
              onPress={() => {
                handleButton(button);
              }}
            />
          </View>
        ))}
      </View>
    </View>
  );
};

export default StartingScreen;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttons_container: {
    margin: 10,
  },
});
