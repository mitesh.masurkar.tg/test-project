import React, {useState} from 'react';
import {Alert, StyleSheet, Text, TextInput, View} from 'react-native';
import CustomButton from '../components/CustomButton';
import CustomTextInput from '../components/CustomTextInput';

const EditDetail = (prop) => {
  const [text, settext] = useState('Mitesh');
  console.log(prop.route);
  const {button} = prop.route.params;
  const submitForm = () => {
    Alert.alert(
      'Successful',
      button + ' Edited Sucessfully',
      [{text: 'OK', onPress: () => {}}],
      {cancelable: false},
    );
  };
  return (
    <View style={styles.container}>
      <View>
        <CustomTextInput
          title="Name"
          value={text}
          changeText={(text) => {
            settext(text);
          }}
        />
        <CustomTextInput
          title="Number"
          value={text}
          keyboardType="phone-pad"
          changeText={(text) => {
            settext(text);
          }}
        />
        <CustomTextInput
          title="Email"
          value={text}
          keyboardType="email-address"
          changeText={(text) => {
            settext(text);
          }}
        />
        <CustomTextInput
          title="Address"
          value={text}
          changeText={(text) => {
            settext(text);
          }}
        />
      </View>
      <View style={styles.button}>
        <CustomButton
          title="submit"
          style={styles.custbutton}
          onPress={submitForm}
        />
      </View>
    </View>
  );
};

export default EditDetail;

const styles = StyleSheet.create({
  container: {
    margin: 10,
    height: '100%',
  },
  button: {
    alignItems: 'center',
    width: '100%',
  },
});
